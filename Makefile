build:
	go build -o gocopy cmd/gocopy/*

test-build:
	go build -o gocopy.test cmd/gocopy/*

lint:
	golangci-lint run --enable-all cmd/...

test:
	go clean -testcache ./...
	go test -v ./...
