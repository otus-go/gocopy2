package tests

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"testing"
	"github.com/sergi/go-diff/diffmatchpatch"
)

const binaryName = "gocopy.test"

// fixture, build executable
func TestMain(m *testing.M) {
	err := os.Chdir("..")
	if err != nil {
		fmt.Printf("could not change dir: %v", err)
		os.Exit(1)
	}
	makeSubprocess := exec.Command("make", "test-build")
	err = makeSubprocess.Run()
	if err != nil {
		fmt.Printf("could not make binary for %s: %v", binaryName, err)
		os.Exit(1)
	}
	exitCode := m.Run()
	_ = os.Remove(binaryName)
	os.Exit(exitCode)
}

func TestCli(t *testing.T) {
	dir, _ := os.Getwd()

	tests := []struct{
		name string
		args []string
		fixture string
	}{
		{
			"no args",
			[]string{},
			"noargs.golden",
		},
		{
			"negative args",
			[]string{"-offset", "-1", "-limit", "-1", "-chunk", "-1"},
			"negative.golden",
		},
		{
			"success case",
			[]string{"-from", "/dev/urandom", "-to", "/dev/null", "-limit", "2048"},
			"success.golden",
		},
	}
	dmp := diffmatchpatch.New()
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			proc := exec.Command(path.Join(dir, binaryName), testCase.args...)
			output, _ := proc.CombinedOutput()
			expected, _ := ioutil.ReadFile(path.Join("tests", testCase.fixture))
			// save this for debug purposes
			//fmt.Println(string(output))
			//fmt.Println(string(expected))
			if bytes.Compare(output, expected) != 0 {
				diffs := dmp.DiffMain(string(output), string(expected), false)
				t.Errorf("unexpected output:\n%s", dmp.DiffPrettyText(diffs))
			}
		})
	}
}
