package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	// drop flags to disable datetime output
	log.SetFlags(0)

	args := newArgs()
	args.init()
	args.parse()
	err := args.validate()
	if err != nil {
		log.Fatalf("%s\n", err)
	}

	// open source file, check error and defer closing
	sourceFile, err := os.Open(args.from)
	if err != nil {
		log.Fatalf("Unable to open source file: %s\n", err)
	}
	defer sourceFile.Close()

	// get file stat and check offset
	fileInfo, err := sourceFile.Stat()
	fileSize := fileInfo.Size()
	if err != nil {
		log.Fatal("Unable to stat source file\n")
	}

	// set cursor
	_, err = sourceFile.Seek(args.offset, 0)
	if err != nil {
		log.Fatalf("Unable to set offset, error: %s\n", err)
	}

	// open destination file
	destinationFile, err := os.OpenFile(args.to, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatalf("Unable to open destination file: %s\n", err)
	}
	defer destinationFile.Close()

	// define limit
	var limit int64
	if args.limit > 0 {
		limit = args.limit
	} else {
		limit = fileSize
	}

	// copy files
	_, err = chunkedCopy(destinationFile, sourceFile, limit, args.chunk)
	if err != nil && err != io.EOF {
		log.Fatalf("%s\n", err)
	}
	fmt.Println()
}

func chunkedCopy(dest io.Writer, source io.Reader, limit, chunk int64) (int64, error) {
	var written int64
	for written < limit {
		bytes, err := io.CopyN(dest, source, chunk)
		written += bytes
		printProgress(written, limit)
		if written+chunk > limit {
			chunk = limit - written
		}
		if err != nil {
			return written, err
		}
	}
	return written, nil
}

func printProgress(done, limit int64) {
	if limit == 0 {
		fmt.Printf("\r\t %d bytes are copied", done)
	}
	progress := float64(done)/float64(limit) * 100
	fmt.Printf("\r\t %.0f%% - %d from %d bytes are copied", progress, done, limit)
}
