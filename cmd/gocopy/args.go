package main

import (
	"errors"
	"flag"
	"strings"
)

type args struct {
	from     string // read from
	to       string // write to
	offset   int64  // offset bytes will be skipped from source beginning
	limit    int64  // bytes to write
	chunk    int64  // read and write chunk bytes at a time
	messages []string
}

func (a *args) init() {
	flag.StringVar(&a.from, "from", "", "source file, required")
	flag.StringVar(&a.to, "to", "", "destination file, required")
	flag.Int64Var(&a.offset, "offset", 0, "bytes, skipped from the beginning of the source file. 0 by default")
	flag.Int64Var(&a.limit, "limit", 0, "maximum bytes, unlimited by default")
	flag.Int64Var(&a.chunk, "chunk", 512, "read and write chunk bytes at a time")
}

func (a *args) parse() {
	flag.Parse()
}

func newArgs() args {
	return args{
		messages: make([]string, 0, 4),
	}
}

func (a *args) validate() error {
	if a.from == "" {
		a.messages = append(a.messages, "\t - argument '-from' is required")
	}

	if a.to == "" {
		a.messages = append(a.messages, "\t - argument '-to' is required")
	}

	if a.offset < 0 {
		a.messages = append(a.messages, "\t - argument '-offset' must be non negative integer")
	}

	if a.limit < 0 {
		a.messages = append(a.messages, "\t - argument '-limit' must be non negative integer")
	}

	if a.chunk <= 0 {
		a.messages = append(a.messages, "\t - argument '-chunk' must be positive integer")
	}

	if len(a.messages) > 0 {
		return errors.New(strings.Join(a.messages, ",\n"))
	}

	return nil
}
